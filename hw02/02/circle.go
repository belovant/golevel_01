// программа, вычисляющая диаметр и длину окружности по заданной площади круга
package main

import (
	"fmt"
	"math"
)

func main() {
	var x float64

	fmt.Print("Введите площадь круга: ")
	_, err := fmt.Scanln(&x)

	if err != nil {
		fmt.Println("ошибка ввода: ", err)
	} else {
		fmt.Printf("Диаметр: %.2f\n", 2*math.Sqrt((x/math.Pi)))
		fmt.Printf("Длина окружности: %.2f", 2*math.Sqrt((x*math.Pi)))
	}
}
