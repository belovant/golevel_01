// разбор трехзначного числа на сотни, десятки и единицы
package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	var x, rank int

	fmt.Print("Введите целое трехзначное число: ")
	_, err := fmt.Scanln(&x)

	if err != nil {
		log.Printf(err.Error())
		os.Exit(1)
	}

	if (x >= 100) && (x < 1000) {
		for i := 100; i >= 1; i = i / 10 {
			rank = x / i
			fmt.Println(rank)
			x = x - rank*i
		}
	} else {
		fmt.Println("Введено не трехзначное число!")
	}
}
