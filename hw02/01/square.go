// программа для вычисления площади прямоугольника
package main

import (
	"fmt"
)

func main() {
	var x, y float32
	fmt.Print("Введите длины сторон прямоугольника (разделенные пробелом): ")
	_, err := fmt.Scanln(&x, &y)

	if err != nil {
		fmt.Println("ошибка ввода: ", err)
	} else {
		fmt.Printf("Площадь: %.2f", RectangleSquare(x, y))
	}
}

func RectangleSquare(x float32, y float32) float32 {
	return x * y
}